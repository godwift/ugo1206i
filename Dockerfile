FROM kasmweb/ubuntu-focal-dind:1.13.1
USER root

ENV HOME /home/kasm-default-profile
ENV STARTUPDIR /dockerstartup
ENV INST_SCRIPTS $STARTUPDIR/install
WORKDIR $HOME

######### Customize Container Here ###########

RUN  wget -c https://go.dev/dl/go1.20.6.linux-arm64.tar.gz \
    && rm -rf /usr/local/go && tar -C /usr/local -xzf go1.20.6.linux-arm64.tar.gz \
    && echo "export PATH=$PATH:/usr/local/go/bin" |  tee ~/.bashrc


######### End Customizations ###########

RUN chown 1000:0 $HOME
RUN $STARTUPDIR/set_user_permission.sh $HOME

ENV HOME /home/kasm-user
WORKDIR $HOME
RUN mkdir -p $HOME && chown -R 1000:0 $HOME

USER 1000
